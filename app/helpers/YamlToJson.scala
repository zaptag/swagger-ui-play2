package helpers

import java.io.InputStream

import com.fasterxml.jackson.core.`type`.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory

trait YamlToJson {
  private lazy val jsonMapper = new ObjectMapper()
  private lazy val yamlMapper = new ObjectMapper(new YAMLFactory())

  private lazy val typeReference = new TypeReference[java.util.Map[String, Object]]() {}

  def toJsonBytes(stream: InputStream): Array[Byte] = {
    val contents = yamlMapper.readValue(stream, typeReference)
      .asInstanceOf[java.util.Map[String, Object]]
    jsonMapper.writeValueAsBytes(contents)
  }
}

object YamlToJson extends YamlToJson
