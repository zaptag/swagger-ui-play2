package controllers.swaggerui

import controllers.{Assets, WebJarAssets}

object SwaggerWebJarAssets extends WebJarAssets(Assets)
