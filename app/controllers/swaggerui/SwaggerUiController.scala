package controllers.swaggerui

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import controllers.WebJarAssets
import helpers.YamlToJson
import play.api.Play
import play.api.Play.current
import play.api.libs.iteratee.Enumerator
import play.api.mvc.{Action, Controller}

import scala.concurrent.ExecutionContext.Implicits.global

object SwaggerUiController extends Controller {

  sealed trait SwaggerSpecFormat
  case object JsonSpec extends SwaggerSpecFormat
  case object YamlSpec extends SwaggerSpecFormat

  lazy val jsonMapper = new ObjectMapper()
  lazy val yamlMapper = new ObjectMapper(new YAMLFactory())

  lazy val config = Play.configuration

  // default is defined in resource.conf
  lazy val specFile = config.getString("swaggerui.spec.file").get

  lazy val specFormat = config.getString("swaggerui.spec.format") match {
    case Some("yaml") => YamlSpec
    case _ => JsonSpec
  }

  def ui = Action {
    val indexLoc = WebJarAssets.locate("swagger-ui", "index.html")

    val indexUrl = controllers.swaggerui.routes.SwaggerWebJarAssets.at(indexLoc).url
    val specUrl = controllers.swaggerui.routes.SwaggerUiController.spec().url

    val redirectUrl = indexUrl + "?url=" + specUrl
    Redirect(redirectUrl)
  }

  def spec = Action {
    (Play.current.resourceAsStream(specFile), specFormat) match {
      case (Some(in), JsonSpec) =>
        Ok.chunked(Enumerator.fromStream(in))
          .withHeaders("content-type" -> "application/json")
      case (Some(in), YamlSpec) =>
        Ok(YamlToJson.toJsonBytes(in))
          .withHeaders("content-type" -> "application/json")
      case (_, _) =>
        NotFound("Not Found")
    }
  }
}
