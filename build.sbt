import play.PlayImport.PlayKeys._

name := "swagger-ui-play2"

organization := "ph.com.zap"

version := "0.1-SNAPSHOT"

isSnapshot := true

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  "org.webjars" %% "webjars-play" % "2.3.0-2",
  "org.webjars" % "swagger-ui" % "2.1.8-M1",
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-yaml" % "2.4.0"
)

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

generateRefReverseRouter := false

publishArtifact in Test := false

publishMavenStyle := false

publishTo := Some {
  val repo = if (isSnapshot.value)
    "http://repo.z4p.me/artifactory/libs-snapshot-local"
  else
    "http://repo.z4p.me/artifactory/libs-release-local"
  Resolver.url("zap-internal", url(repo))(Resolver.ivyStylePatterns)
}

packagedArtifacts += ((artifact in playPackageAssets).value -> playPackageAssets.value)
